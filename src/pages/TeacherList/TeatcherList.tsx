import React from 'react';
import PageHeader from '../../components/PageHeader/index';

import whatsappIcon from '../../assets/images/icons/whatsapp.svg';


import './styles.css';

function TeatcherList() {
    return (
        <div id="page-teacher-list" className="container">
            <PageHeader title="Estes são os proffys disponiveis.">
                <form id="search-teachers">
                    <div className="input-block">
                        <label htmlFor="subject">Matéria</label>
                        <input type="text" id="subject" />
                    </div>
                    <div className="input-block">
                        <label htmlFor="subject">Dia da Semana</label>
                        <input type="text" id="week_day" />
                    </div>
                    <div className="input-block">
                        <label htmlFor="subject">Hora</label>
                        <input type="text" id="time" />
                    </div>
                </form>
            </PageHeader>

            <main>
                <article className="teacher-item">
                    <header>
                        <img src="https://scontent-gru2-1.cdninstagram.com/v/t51.2885-19/s150x150/96385857_270220734132698_741055169080328192_n.jpg?_nc_ht=scontent-gru2-1.cdninstagram.com&_nc_ohc=1D-R49U2YcUAX_MsZuH&oh=43d0b0ca5c1eb475a606192efe48bac3&oe=5F54E871" alt="Valdeir Silva" />
                        <div>
                            <strong>Valdeir Silva</strong>
                            <span>Arquiteto de Software</span>
                        </div>
                    </header>
                        <p>
                        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard 
                        <br/><br/>
                        McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of 
                        </p>
                    <footer>
                        <p>Preco/Hora
                            <strong>R$ 100,00</strong>
                        </p>
                        <button type="button">
                            <img src={whatsappIcon} alt="Whatsapp"/>
                            Entrar em contato
                        </button>
                    </footer>
                </article>
            </main>
        </div>
    );
}

export default TeatcherList;